#!/usr/bin/env bash
set -eu -o pipefail

# ensure_line <FILE> <LINE>
ensure_line() {
    if ! grep -sqF -- "$2" "$1"; then
        printf '%s\n' "$2" >> "$1"
    fi
}

# Add the ansible user
if ! getent passwd ansible &> /dev/null; then
    useradd -m ansible -s /bin/bash
fi

# Set up key-based ssh access
mkdir -p ~ansible/.ssh
chown "ansible:$(id -g ansible)" ~ansible/.ssh
chmod 0700 ~ansible/.ssh
ensure_line ~ansible/.ssh/authorized_keys \
            'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+J0puhRQksmrEQgL6pkb4DW/q5eap2sY6GKMegPb6Jy644yBO8ADsTctNG8Rt4uJE62Uct1f2WXAn4dzRDMhSgvQO0qJ9LO4TxLiyAQe0Wiq7R6joORH0ZfpN222epaK2ZhxKj2UCE2n3Xr6wxmYDH3RoE2whL8QtsBp4QHe9vPbiszm2vMNVIQ3YteJ8NVNX9DGEAiBoX89jUT8Gw9IBTmNDaI1Rw/w9S9JGczBF8vfzB2zCCMQWhk/oTB7hVH/Y9uG+M8rq8X1vsxqGaeonvTZUzjeLCQPYDx0MHLTmVtY8DmaL/9aXTrYiVIxffiedWimQqL14OdygQOkYfY2X winston@winston-one' 
chmod 0600 ~ansible/.ssh/authorized_keys
chown "ansible:$(id -g ansible)" ~ansible/.ssh/authorized_keys

# Set up password-less sudo
ensure_line /etc/sudoers.d/ansible 'ansible ALL = NOPASSWD: ALL' 

# Prevent password based logins
ensure_line /etc/ssh/sshd_config 'PasswordAuthentication no'
ensure_line /etc/ssh/sshd_config 'AllowUsers ansible'
systemctl reload ssh

printf 'Server should be set up for remote ansible deployment!  Have fun!\n'
